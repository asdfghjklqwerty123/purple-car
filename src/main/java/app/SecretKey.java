package app;

public class SecretKey {
    /**
     * A secret private key
     */
    private String value;

    public SecretKey(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

}
